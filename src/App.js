
import './App.css';
import {useState} from 'react';

// import {Container} from 'react-bootstrap';
// import {Fragment} from 'react';
//importing AppNavBar function from the AppNavBar.js
import AppNavBar from './components/AppNavBar.js';
/*import Banner from './Banner.js';
import Highlights from './Highlights.js';*/
import Home from './pages/Home.js';
import Courses from './pages/Courses.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import PageNotFound from './pages/PageNotFound.js';
//import modules from react-router-dom for the routing

//import the UserProvider
import {UserProvider} from './UserContext.js';

import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';

function App() {

  const [user, setUser] = useState(localStorage.getItem('email'));

  const unSetUser = () => {
    localStorage.clear();
  }
 
 //Storing information in a context object is done by providing the information using the corresponding 'Provider' and passing information thru the prop value;
  //all information/data provided to the Provider component can be access later on from the context object properties

  return (
    <UserProvider value = {{user, setUser, unSetUser}}>
      <Router>
      <AppNavBar/>
      <Routes>

          <Route path ='/' element = {<Home/>}/>
          <Route path ='/courses' element = {<Courses/>}/>
          <Route path ='/login' element = {<Login/>}/>
          <Route path ='/register' element = {<Register/>}/>
          <Route path ='/logout' element = {<Logout/>}/>
          <Route path='*' element={<PageNotFound/>}/>


      </Routes>
    </Router>
    </UserProvider>
  );
}

export default App;
