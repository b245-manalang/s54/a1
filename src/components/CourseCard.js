import {Row, Col, Button, Card} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext.js'
import {Link} from 'react-router-dom';


export default function CourseCard({courseProp}){

	const {name, description, price} = courseProp;

		//The curly braces are used for props to signify that we are providing information using expressions

	//Use the state hook for this component to be able to store its state
	//States are used to keep track of information related to individual components

		// const [getter, setter] = useState(initialGetterValue);

	const [enrollees, setEnrollees] = useState(0);
	//add new state that will declare whether the button is disabled or not

	const [isDisabled, setIsDisabled] = useState(false);

	//initial value of enrollees state
	/*	console.log(enrollees);

		setEnrollees(1);
		console.log(enrollees);
*/
	const [seats, setSeats] = useState(30);

	const {user, setUser} = useContext(UserContext);
	function enroll(){
		if (seats > 1 && enrollees < 29 ){
			setEnrollees(enrollees +1)
			setSeats(seats -1)
		
		}else{
			alert('Congratulations for making it to the cut!')
			setEnrollees(enrollees +1)
			setSeats(seats -1)
		}
	}


	//Define a 'useEffect' hook to have the 'CourseCard' component to perform a certain task

	//This will run automatically.
	//Syntax:
		//useEffect(sideEffect/function, [dependencies]);

	//sideEffect/function - it will run on the first load and will reload depending on the dependency array

	useEffect(()=> {
		if(seats === 0){
			setIsDisabled(true);
		}
	}, [seats]);

	return(
		<Row className = 'mt-5'>
			<Col>
		 		<Card>				      
				    <Card.Body>

		                <Card.Title>{name}</Card.Title>
		                <Card.Subtitle>Description:</Card.Subtitle>
		                <Card.Text>{description}</Card.Text>
		                <Card.Subtitle>Price:</Card.Subtitle>
		                <Card.Text>Php {price}</Card.Text>
		                <Card.Subtitle>Enrollees:</Card.Subtitle>
		                <Card.Text>{enrollees}</Card.Text>
		                 <Card.Subtitle>Available Seats:</Card.Subtitle>
		                <Card.Text>{seats}</Card.Text>
		                
		                {
		                user ?
		                
		                <Button onClick = {enroll} disabled = {isDisabled}>Enroll Now!</Button>
		                :
		                <Button as = {Link} to = '/login'>Login</Button>
		            	}

				    </Card.Body>	
				</Card>
			</Col>
		</Row>
	  	);
	}